# -*- coding: utf-8 -*-
from __future__ import print_function
from __future__ import division

"""
Created on Sun Sep 13 10:18:35 2015

@author: heller
"""

# This file is part of the Ambisonic Decoder Toolbox (ADT).
# Copyright (C) 2015  Aaron J. Heller
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys

from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import numpy as np

# Note that mayavi via conda is screwed up, had to do
#   conda install mayavi
#   conda install vtk=6.3
# but I still get errors in this script. 

try:
    from mayavi import mlab
    mayavi_available = True
except ImportError:
    mayavi_available = False

from matplotlib import cm

from symYlm import Ylm_function


def plot_Ylm(l, m, use_mayavi=True):

    u = np.linspace(0, 2 * np.pi, 100)
    v = np.linspace(-np.pi/2, np.pi/2, 100)

    x = np.outer(np.cos(u), np.cos(v))
    y = np.outer(np.sin(u), np.cos(v))
    z = np.outer(np.ones(np.size(u)), np.sin(v))

    f = Ylm_function(l, m, normalization='semi', four_pi=True)
    c = f(x, y, z)
    ca = np.abs(c)

    if mayavi_available and use_mayavi:
        #  http://docs.enthought.com/mayavi/mayavi/auto/mlab_helper_functions.html#mayavi.mlab.mesh
        s = mlab.mesh(ca*x, ca*y, ca*z,
                      scalars=c,
                      line_width=0,
                      representation='surface')
        mlab.axes()
        mlab.title('Ylm(%d,%d)' % (l, m))
        mlab.colorbar(object=s, nb_labels=7)
        mlab.show()

    else:
        # normalize c0 to range 0 .. 1
        c0 = c - c.min()
        c0 = c0 / c0.max()

        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')

        ax.plot_surface(ca*x, ca*y, ca*z,
                        rstride=1, cstride=1,
                        antialiased=True,
                        linewidth=0,
                        facecolors=cm.jet(c0)
                        )
        map = cm.ScalarMappable(cmap=cm.jet)
        map.set_array(c)
        plt.colorbar(map)

        # can't figure out how to get mpl to autoscale the axes,
        #  and keep the aspect ratio the same.
        ax.set_xlim3d(-1, 1)
        ax.set_ylim3d(-1, 1)
        ax.set_zlim3d(-1, 1)
        ax.set_aspect('equal')

        ax.set_xlabel('x -->')
        ax.set_ylabel('y -->')
        ax.set_zlabel('z -->')

        ax.set_title('Ylm(%d,%d)' % (l, m))
        plt.show()


if __name__ == '__main__':
    if len(sys.argv) > 2:
        plot_Ylm(int(sys.argv[1]), int(sys.argv[2]))
    else:
        print('need l and m')
