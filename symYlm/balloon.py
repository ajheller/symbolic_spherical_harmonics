#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Aug  3 16:10:04 2018

@author: heller
"""
import numpy as np

# plotly is not available via conda
# use pip install plotly to install
import plotly
import plotly.graph_objs as go
from plotly import tools

import sympy
import symYlm

#from symYlm import Ylm_function


def grid_az_el(resolution=180):
    u = np.linspace(-np.pi, np.pi, (2 * resolution) + 1)
    v = np.linspace(-np.pi/2, np.pi/2, resolution + 1)

    el, az = np.meshgrid(v, u)
    x = np.cos(az) * np.cos(el)
    y = np.sin(az) * np.cos(el)
    z = np.sin(el)

    return x, y, z, az, el


def balloon_ex(ex, title="",
               sph_vars=(symYlm.theta, symYlm.phi),
               cart_vars=(symYlm.x, symYlm.y, symYlm.z),
               res=180):

    x, y, z, az, el = grid_az_el(res)

    symbs = ex.atoms(sympy.Symbol)

    if symbs.issubset(cart_vars):
        f = sympy.lambdify(cart_vars, ex, modules='numpy')
        c = f(x, y, z) * np.ones_like(x)
        c_text = np.vectorize(lambda u, v, w, c:
                              "r: %.3f<br>x: %.3f<br>y: %.3f<br>z: %.3f"
                              % (c, u, v, w))(x, y, z, c)

    elif symbs.issubset(sph_vars):
        f = sympy.lambdify(sph_vars, ex, modules='numpy')
        c = f(az, el) * np.ones_like(x)
        c_text = np.vectorize(lambda u, v, c:
                              "r: %.3f<br>a: %.1f<br>e: %.1f"
                              % (c, u, v))(az*180/np.pi, el*180/np.pi, c)
    else:
        raise ValueError("Input expression has unrecognized variables: " +
                         str(tuple(symbs.difference(set(sph_vars+cart_vars)))))

    ca = np.abs(c)

    data = [go.Surface(x=ca*x, y=ca*y, z=ca*z,
                       surfacecolor=c,
                       customdata=c,
                       text=c_text,
                       hoverinfo='text',
                       colorscale='Jet')]

    poly = str(ex)
    poly = poly.replace("\n", "<br>").replace(" ", "&nbsp;")
    #layout = go.Layout(title="Ylm(%d,%d)<br><br>%s" % (l, m, str(poly)))
    layout = go.Layout(title=title)

    plotly.offline.plot({'data': data, 'layout': layout},
                        filename="plotly/tmp.html")



def balloon_lm(l, m, res=180):
    x, y, z, az, el = grid_az_el(res)

    # SN3D and 4-pi have the fewest constants

    f = Ylm_function(l, m, normalization=symYlm.Norm.semi, four_pi=True)
    c = f(x, y, z) * np.ones(np.shape(x))
    ca = np.abs(c)

    # the create the hoverover text
    c_text = np.vectorize(lambda u, v, c: "r: %.2f<br>a: %.1f<br>e: %.1f"
                          % (c, u, v))(az*180/np.pi, el*180/np.pi, c)

    data = [go.Surface(x=ca*x, y=ca*y, z=ca*z,
                       surfacecolor=c,
                       customdata=c,
                       text=c_text,
                       hoverinfo='text',
                       colorscale='Jet')]

    poly = str(symYlm.Ylm(l, m, sph=True,
                          normalization=symYlm.Norm.semi,
                          four_pi=True))
    poly = poly.replace("\n", "<br>").replace(" ", "&nbsp;")
    layout = go.Layout(title="Ylm(%d,%d)<br><br>%s" % (l, m, str(poly)))

    plotly.offline.plot({'data': data, 'layout': layout},
                        filename="plotly/Ylm_%d_%d.html" % (l, m))


def balloon_trace(l, m, grid):
    x, y, z, az, el = grid

    f = Ylm_function(l, m, normalization=symYlm.Norm.semi, four_pi=True)
    c = f(x, y, z) * np.ones(np.shape(x))
    ca = np.abs(c)

    # the create the hoverover text
    c_text = np.vectorize(lambda u, v, c: "r: %.2f<br>a: %.1f<br>e: %.1f"
                          % (c, u, v))(az*180/np.pi, el*180/np.pi, c)

    return dict(type='surface', x=ca*x, y=ca*y, z=ca*z,
                surfacecolor=c,
                cmin=-1.2,
                cmax=1.2,
                customdata=c,
                text=c_text,
                hoverinfo='text',
                colorscale='Jet')

####
#
# classic SH tree diagram
if False:
    # 3D-surface subplut examples:
    #     https://plot.ly/python/3d-subplots/#3d-surface-subplots
    n_rows = 4
    n_cols = 2 * n_rows + 1
    grid = grid_az_el(resolution=60)
    acn = 0
    fig = tools.make_subplots(rows=n_rows, cols=n_cols,
                              specs=[[{'is_3d': True}
                                      for i in range(n_cols)]
                                     for j in range(n_rows)])
    for l in range(n_rows):
        for m in range(-l, l+1):
            fig.append_trace(
                    balloon_trace(l, m, grid),
                    l+1, m+l+1+(n_rows-l))

    fig['layout'].update(height=1200, width=2000, title='Spherical Harmonics')

    plotly.offline.plot(fig,
                        filename="tree.html")


# do a single plot
if False:
    sh_l = 15
    sh_m = -4
    balloon(sh_l, sh_m)

# do all the plots, on separate pages
if False:
    max_l = 5
    for l in range(max_l+1):
        for m in range(-l, l+1):
            balloon(l, m)

