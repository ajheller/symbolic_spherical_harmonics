# -*- coding: utf-8 -*-
from __future__ import division, print_function

"""
Created on Sat Aug 15 16:04:15 2015

@author: heller
"""

# =============================================================================
# This file is part of the Ambisonic Decoder Toolbox (ADT).
# Copyright (C) 2015  Aaron J. Heller
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# =============================================================================

from sympy import integrate, simplify, lambdify, N
from sympy import cos, pi
#import sympy.mpmath as mpm
import mpmath as mpm # mpmath is not installed with anaconda
import numpy

# uses in unit tests
from sympy import init_printing, pprint, latex
from contextlib import closing
import multiprocessing as mp
import time

from symYlm import theta, phi, x
from symYlm import Ylm, legendre_poly, Norm

# confirm that the generated spherical harmonics are orthonormal
#  that means that inner product over the sphere should be 1 if
#  l1, m1 = l2, m2, 0 otherwise.


def inner_product_over_sphere_symbolic(ex1, ex2):
    "inner product of ex1 and ex2 over the surface of the sphere"
    return integrate(simplify(ex1 * ex2 * cos(phi)),
                     (theta, 0, 2*pi),
                     (phi, -pi/2, pi/2))


def inner_product_over_sphere_numeric(ex1, ex2):
    pi = mpm.pi
    integrand = simplify(ex1 * ex2 * cos(phi))
    fn = lambdify((theta, phi), integrand)
    return mpm.quadgl(fn, (0, 2*pi), (-pi/2, pi/2), maxdegree=1000)


def inner_product_over_sphere(ex1, ex2, symbolic=True):
    if symbolic:
        retval = inner_product_over_sphere_symbolic(ex1, ex2)
    else:
        retval = inner_product_over_sphere_numeric(ex1, ex2)
    return retval


# cache for spherical harmonic expressions
_ex_cache = {}


def Ylm_cache(l, m):
    key = (l, m)
    ex = _ex_cache[key] = _ex_cache.get(key, Ylm(l, m, sph=True))
    return ex

#
# use mutiprocessing to take advantage of multiple cores

inner_product_symbolic_max_degree = 2


def compute_pair_1arg((l1, m1, l2, m2)):
    start = time.time()
    symbolic = max(l1, l2) <= inner_product_symbolic_max_degree
    q = inner_product_over_sphere(Ylm_cache(l1, m1), Ylm_cache(l2, m2),
                                  symbolic=symbolic)
    return l1, m1, l2, m2, q, time.time()-start


def analyze_pair_1arg((l1, m1, l2, m2, q, t)):

    epsilon = 1e-10

    if l1 == l2 and m1 == m2:
        ok = abs(q-1) < epsilon
    else:
        ok = abs(q) < epsilon

    print("(%d, %+d), (%d, %+d), % 0.6e, %0.2f  %s"
          % (l1, m1, l2, m2, q, t,
             "PASS" if ok else "***FAIL***"))

    assert ok

    return ok


def lm_range(*args):
    return [(l, m)
            for l in range(*args)
            for m in range(-l, l+1)]


def cartesian_square(s, commutative=True):
    return ((s[i], s[j])
            for i in range(len(s))
            for j in range(i+1 if commutative else len(s)))


def ortho_check_pool(lms, all=False):

    # set up the pool, one process per cpu
    n_cores = mp.cpu_count()
    with closing(mp.Pool(n_cores)) as pool:
        # dispatch workers
        if all:
            results = [pool.apply_async(compute_pair_1arg,
                                        args=((l1, m1, l2, m2),),
                                        callback=analyze_pair_1arg)
                       for ((l1, m1), (l2, m2)) in cartesian_square(lms)]
        else:
            results = [pool.apply_async(func=compute_pair_1arg,
                                        args=((l, m, l, m),),
                                        callback=analyze_pair_1arg)
                       for l, m in lms]
    n_results = len(results)
    print("Performing %d tests with %d cores ..." % (n_results, n_cores))
    start = time.time()

    # wait for results to arrive, analyze and summarize
    if False:
        n_results = len(results)

        while results:
            print("# results remaining:", len(results), '/', n_results)
            for result in results:
                if result.ready():
                    analyze_pair_1arg(result.get())
                    results.remove(result)
            sys.stdout.flush()
            time.sleep(5)
    else:
        for r in results:
            r.wait()

    print("Total time = %f secs." % (time.time() - start))


# precision check
#  Evaluating the expressions for the spherical harmonics requires greater than
#  double precision float above degree 29.  The problem is that the
#  coefficients of the associated legendre polynomials get very large.  The
#  test here is that for unnormalized Legendre polynomials, P_l(1) = 1.

def precision_check(degree):
    p = legendre_poly(degree, 0, normalization=Norm.none)
    # evalf returns the exact answer, i.e., 1 exactly
    ve = p.evalf(subs={x: 1}, maxn=10)
    # lambdify with different packages... apparently lambdify is kind of broken
    # becayse it calls eval() which rounds constants to doubles regardless of
    # the underlying package.
    # see https://github.com/sympy/sympy/issues/8818
    vl = [(m, lambdify('x', N(p, 50), modules=m)(numpy.float128(1)))
          for m in ("math", "mpmath", "numpy", "numexpr", "sympy")]
    return degree, ve, vl


# The idea here is to see how many digits of precision are needed for a given
# degree, but it seems like maxn doesn't effect the calculation. Another SymPy
# mystery.
def precision_required(degree):
    p = legendre_poly(degree, 0, normalization=Norm.none)
    n_low = 7
    n_high = 100
    while n_high - n_low > 1:
        n_test = int(round((n_high+n_low)/2))
        pa = N(p, n_test)
        v = pa.evalf(subs={x: 1}, maxn=n_test)
        if v.epsilon_eq(1):
            n_high = n_test
        else:
            n_low = n_test

    return (degree,
            n_high, p.evalf(subs={x: 1}, maxn=n_high),
            n_low, p.evalf(subs={x: 1}, maxn=n_low))


def unit_test(*argv):
    argc = len(argv)

    if argc > 0 and argc < 3:
        lms = lm_range(*argv)
    else:
        lms = lm_range(3)

    if argc >= 3:
        init_printing(use_latex=True)
        for l, m in lms:
            pprint([(l, m), Ylm_cache(l, m)])
            print()

    ortho_check_pool(lms, all=True)


if __name__ == '__main__':
    import sys
    unit_test(*sys.argv[1:])
