#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import division
from __future__ import print_function

"""
Created on Fri Mar 13 17:27:44 2015

@author: heller
"""
# =============================================================================
# This file is part of the Ambisonic Decoder Toolbox (ADT).
# Copyright (C) 2015  Aaron J. Heller
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# =============================================================================

from sympy import re, im, I, Rational, Integer
from sympy import symbols
from sympy import diff, prod, factorial
from sympy import sqrt, pi, cos, sin
from sympy import simplify, nsimplify, expand, lambdify, expand_trig
import sympy.polys.polyfuncs as sympoly


import sympy.printing as sympr

# import other files in this module
import faustcode as fc
import max_Ylm as my
#import balloon

# enum is not available in anaconda distro
try:
    from enum import IntEnum
except ImportError:
    class IntEnum:
        pass

# this is needed by sympy.printing.
_use_theano = False
if _use_theano:
    try:
        import theano
        theano_available = True
    except ImportError:
        theano_available = False
    except Exception as ex:
        print(ex)
        print("\nNOTE: Current version of theano in Anaconda is broken.\n " +
              "See https://github.com/Theano/Theano/issues/6645\n " +
              "Disabling use of theano")
        theano_available = False
else:
    theano_available = False

############
#
# Fully normalized (N3D) real spherical harmonics in Cartesian coordinates
#  removed +/- in x +/- yi term because real SH always have positive azimuthal
#  frequency (m) and  + and - selects between cos and sin components
#  See eqns 4-6 in [1] for derivation.
#
#  We assume [x,y,z]^T is a unit vector
#
#  [1]    H. B. Schlegel and M. J. Frisch, “Transformation Between Cartesian
#         and Pure Spherical Harmonic Gaussians,” International Journal of
#         Quantum Chemistry, vol. 54, pp. 83–87, 1995.


x, y, z = symbols('x y z', real=True)
theta, phi, rho = symbols('theta phi rho', real=True)


def factorial_quotient(num, dem):
    "returns num!/dem!, but computed to prevent overflow"

    if num < 0 or dem < 0:
        raise ValueError("num and dem should be non-negative")

    if num > dem:
        return Rational(prod(range(dem + 1, num + 1)), 1)
    elif num < dem:
        return Rational(1, prod(range(num + 1, dem + 1)))
    else:
        return Integer(1)


##########################################
# conventions for spherical coordinate systems
#  see https://en.wikipedia.org/wiki/Spherical_coordinate_system
#
# physics:
#   phi is counter-clockwise horizontal angle around the z-axis, 0 is x-axis
#   theta is zenith angle, angle from positive z axis
#   This is the ISO convention
#   Mathematica uses this.
spherical_coordinates_physics = {
    x: cos(phi) * sin(theta),
    y: sin(phi) * sin(theta),
    z: cos(theta)}

spherical_coordinates_iso = spherical_coordinates_physics

# mathematics:
#   theta is counter-clockwise horizontal angle around the z-axis, 0 = x-axis
#   phi is zenith angle, angle from positive z-axis
spherical_coordinates_mathematics = {
    x: cos(theta) * sin(phi),
    y: sin(theta) * sin(phi),
    z: cos(phi)}

# geodesy and ambisonics:
#   theta is azimuth, counter-clockwise horizontal angle around the z-axis,
#     0 = x-axis
#   phi is elevation, angle from x-y plane, positive phi is positive z-axis
#   MATLAB cart2sph and sph2cart use this
spherical_coordinates_azimuth_elevation = {
    x: cos(theta) * cos(phi),
    y: sin(theta) * cos(phi),
    z: sin(phi)}

#
##########################################


def cartesian_to_spherical(ex,
                           convention=spherical_coordinates_azimuth_elevation):
    return simplify(ex.subs(convention))


class Norm(IntEnum):
    __order__ = 'none sch norm max fuma'
    none = 1
    sch = 2
    norm = 3
    max = 4
    fuma = 5
    semi = 2
    SN3D = 2
    full = 3
    N3D = 3

FuMaChannelNames = \
    {"W": (0, 0),
     "X": (1, 1), "Y": (1, -1), "Z": (1,  0),
     "R": (2, 0), "S": (2,  1), "T": (2, -1), "U": (2, 2), "V": (2, -2),
     "K": (3, 0), "L": (3,  1), "M": (3, -1), "N": (3, 2), "O": (3, -2),
     "P": (3, 3), "Q": (3, -3)}


def _check_kwargs(sph=None, four_pi=None, condon_shortley_phase=None,
                  normalization=None):
    pass


def _check_degree_order_args(l, m):
    ll = int(l)
    mm = int(m)
    if l == ll and m == mm:
        l = ll
        m = mm
    else:
        raise ValueError("l and m should be integers")

    if l < 0:
        raise ValueError("l should be non-negative")

    if abs(m) > l:
        raise ValueError("|m| should be <= l")

    return l, m


def legendre_poly(l, m=0,
                  normalization=Norm.none, sph=False,
                  condon_shortley_phase=False,
                  horner=False,
                  **kwargs):

    kwargs['normalization'] = normalization
    kwargs['sph'] = sph
    kwargs['condon_shortley_phase'] = condon_shortley_phase
    _check_kwargs(**kwargs)

    if sph:
        zz = sin(phi)
    else:
        zz = x

    a = (Ylm_norm(l, m, four_pi=True, **kwargs) *
         (1 - zz**2)**Rational(m, 2) *
         Ylm_z(l, m, z_symb=zz, **kwargs))

    if condon_shortley_phase:
        a *= (-1)**abs(m)

    # a = nsimplify(a, rational=True, full=True)
    a = simplify(a)

    if horner:
        a = sympoly.horner(a)

    return a


def Ylm_norm(l, m, normalization=Norm.full, four_pi=False, **kwargs):

    norm_sqr = Integer(1)

    if normalization >= Norm.semi:
        mag_m = abs(m)
        norm_sqr *= factorial_quotient(l - mag_m, l + mag_m)
        if mag_m > 0:
            norm_sqr *= 2

    if normalization >= Norm.norm:
        norm_sqr *= 2 * l + 1

    if normalization >= Norm.max:
        maxS, maxN, exact = my.max_Ylm(l, m)
        norm_sqr /= maxS**2

    # fumaN is maxN, except that (0,0) is sqrt(2)/2
    if normalization is Norm.fuma and l == 0 and m == 0:
        norm_sqr /= 2

    if not four_pi:
        norm_sqr /= 4 * pi

    return sqrt(norm_sqr)


def Ylm_xy(l, m, x_symb=x, y_symb=y, condon_shortley_phase=False, **kwargs):
    "The trigonometric part of the real spherical harmonic"

    a = expand((x_symb + I * y_symb)**abs(m))

    if condon_shortley_phase:
        a *= (-1)**abs(m)

    return im(a) if m < 0 else re(a)


def Ylm_z(l, m, z_symb=z, **kwargs):
    "The Legendre polynomial part of the spherical harmonic"
    return (diff(expand((z_symb**2 - 1)**l), z_symb, l + abs(m)) /
            (2**l * factorial(l)))


def Ylm_cartesian(l, m, **kwargs):
    "Real spherical harmonic as a polynomial in x, y, z; fast API"
    # print(l, m)
    # print(type(l), type(m))
    return (Ylm_norm(l, m, **kwargs) *
            Ylm_xy(l, m, x, y, **kwargs) *
            Ylm_z(l, m, z, **kwargs))


def Ylm(l, m, sph=False, horner=False, **kwargs):
    "Real spherical harmonics of degree l and order m; full-featured API"

    l, m = _check_degree_order_args(l, m)
    _check_kwargs(**kwargs)

    # This workaround is needed because we might get another type of integer,
    # such as numpy.int64, which some versions of SymPy do not recognize see as
    # an integer.  However, just coercing to a Python int would allow
    # non-integer values of l and m, which would return misleading answers.
    ll = int(l)
    mm = int(m)
    if l == ll and m == mm:
        l = ll
        m = mm
    else:
        raise ValueError("l and m should be integers")

    if l < 0:
        raise ValueError("l should be non-negative")

    if abs(m) > l:
        raise ValueError("|m| should be <= l")

    Y = Ylm_cartesian(l, m, **kwargs)

    Y_simp = nsimplify(Y, rational=True, full=True)

    if sph:
        Y_simp = cartesian_to_spherical(Y_simp)
        # Y_simp = expand_trig(Y_simp)

    # http://mathworld.wolfram.com/HornersRule.html
    if horner:
        Y_simp = sympoly.horner(Y_simp)

    return Y_simp


def Ylm_function_subs(l, m, sph=False, **kwargs):
    kwargs['sph'] = sph

    ex = Ylm(l, m, **kwargs)

    if sph:
        sym_theta = theta
        sym_phi = phi

        def f(theta, phi):
            return ex.subs({sym_theta: theta, sym_phi: phi})
    else:
        sym_x = x
        sym_y = y
        sym_z = z

        def f(x, y, z):
            return ex.subs({sym_x: x, sym_y: y, sym_z: z})

    return f


def Ylm_function_evalf(l, m, sph=False, **kwargs):
    kwargs['sph'] = sph

    ex = Ylm(l, m, **kwargs)

    if sph:
        sym_theta = theta
        sym_phi = phi

        def f(theta, phi):
            return ex.evalf(subs={sym_theta: theta, sym_phi: phi})
    else:
        sym_x = x
        sym_y = y
        sym_z = z

        def f(x, y, z):
            return ex.evalf(subs={sym_x: x, sym_y: y, sym_z: z})

    return f


def Ylm_function(l, m, **kwargs):
    "return numpy function for Ylm"

    if kwargs.get("sph", False):
        func_args = (theta, phi)
    else:
        func_args = (x, y, z)

    # Note: lambdify has precision problems:
    #   https://github.com/sympy/sympy/issues/8818
    # even when specifying mpmath, it still uses double floats which means
    # that this craps out for l > 25 (-ish)
    if l <= 20:
        f = lambdify(func_args, Ylm(l, m, **kwargs), modules='numpy')
    else:
        f = lambdify(func_args, Ylm(l, m, **kwargs), modules='mpmath')

    return f


if theano_available:
    def Ylm_theano(l, m, **kwargs):
        "return theano function for Ylm"

        if kwargs.get("sph", False):
            func_args = [theta, phi]
        else:
            func_args = [x, y, z]

        f = sympr.theanocode.theano_function(func_args,
                                             [Ylm(l, m, **kwargs)],
                                             on_unused_input='ignore')
        return f


translation_dict = {'fortran': sympr.fcode,
                    'c': sympr.ccode,
                    'octave': sympr.octave.octave_code,
                    'matlab': sympr.octave.octave_code,
                    'mathematica': sympr.mathematica_code,
                    'javascript': sympr.jscode,
                    'mathml': sympr.mathml,
                    'tree': sympr.print_tree,
                    'pretty': sympr.pretty_print,
                    'faust': fc.faust_code,
                    'latex': sympr.latex,
                    'julia': sympr.julia_code,
                    'none': lambda x: x}


# -------------------- main ----------------------------
if __name__ == '__main__':
    import argparse
    import sys

    parser = argparse.ArgumentParser(
        description=("returns a polynomial experession for the real spherical "
                     "harmonic of the specified degree (l) and order (m)"))

    parser.add_argument("degree", type=int,
                        help="l, degree of the spherical harmonic. l>=0")
    parser.add_argument("order", type=int,
                        help="m, order of the spherical harmonic. |m|<=l")

    parser.add_argument("-v", "--verbose", action="store_true",
                        help="increase output verbosity")
    parser.add_argument("-s", "--spherical", action="store_true",
                        help="use spherical coordinates.")
    parser.add_argument("-c", "--cartesian", action="store_true",
                        help="use Cartesian coordinates. (default)")
    parser.add_argument("--horner", action="store_true",
                        help="use Horner's method to produce a more" +
                        " numerically-stable expression")
    parser.add_argument("--plot", action="store_true",
                        help="produce a plotly plot of the polynomial")

    unit_group = parser.add_mutually_exclusive_group()
    unit_group.add_argument("--four_pi", action="store_true",
                            help="inner product is 4pi")
    unit_group.add_argument("--unity", action="store_true",
                            help="inner product is 1 (default)")

    norm_group = parser.add_mutually_exclusive_group()
    norm_group.add_argument("--orthonormal",
                            action="store_const", const=Norm.full,
                            help="fully orthonormalized (default)")
    norm_group.add_argument("--seminormalized",
                            action="store_const", const=Norm.semi,
                            help="Schmidt seminormalized")
    norm_group.add_argument("--unnormalized",
                            action="store_const", const=Norm.none,
                            help="no normalization")
    norm_group.add_argument("--max", action="store_const", const=Norm.max,
                            help="max gain is 1 (note: may take a while to" +
                                 " compute and result may not exact for l>3")
    norm_group.add_argument("--FuMa", action="store_const", const=Norm.fuma,
                            help="Furse-Malham normalization")

    parser.add_argument("--condon_shortley_phase", action="store_true",
                        help="Use Condon-Shortley Phase (default is off)")

    parser.add_argument("-t", "--translation",
                        choices=sorted(translation_dict.keys()),
                        action="store",
                        default='none')

    # this is a workaround to allow optional arguments that are
    #  negative numbers.  See this post on for more discussion
    #  http://stackoverflow.com/questions/9025204/python-argparse-issue-with-\
    #    optional-arguments-which-are-negative-numbers
    for i, arg in enumerate(sys.argv):
        if (arg[0] == '-') and arg[1].isdigit():
            sys.argv[i] = ' ' + arg

    args = parser.parse_args()

    # default to identity function
    translation_fn = translation_dict.get(args.translation, lambda x: x)

    norm = (args.orthonormal or
            args.seminormalized or
            args.unnormalized or
            args.max or
            args.FuMa or
            Norm.full)

    Y = Ylm(args.degree, args.order,
            sph=args.spherical,
            horner=args.horner,
            normalization=norm,
            four_pi=args.four_pi,
            condon_shortley_phase=args.condon_shortley_phase)

#    if args.plot:
#        balloon.balloon(args.degree, args.order)

    print(translation_fn(Y))
