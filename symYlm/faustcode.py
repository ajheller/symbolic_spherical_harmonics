# -*- coding: utf-8 -*-
"""
Created on Wed Sep  9 18:56:32 2015

@author: heller
"""
# =============================================================================
# This file is part of the Ambisonic Decoder Toolbox (ADT).
# Copyright (C) 2015  Aaron J. Heller
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# =============================================================================

from __future__ import division, print_function

from sympy import __version__ as sympy_version
if sympy_version <= "1.0":
    from sympy.printing.ccode import CCodePrinter
else:
    from sympy.printing.ccode import C99CodePrinter as CCodePrinter

class FaustCodePrinter (CCodePrinter):

    def _print_Rational(self, expr):
        p, q = int(expr.p), int(expr.q)
        return '%d/%d' % (p, q)

    def _print_Pi(self, expr):
        return 'math.PI'

    def _print_Matrix(self, expr):
        return 'MATRIX!'

    def _print_ImmutableMatrix(self, expr):
        return "("+", ".join((faust_code(e) for e in expr))+")"


def faust_code(expr, assign_to=None, **settings):
    return FaustCodePrinter().doprint(expr)


def print_faust_code(expr, **settings):
    print(faust_code(expr, **settings))


def print_faust_vector(assign_to, v, line_end=",\n", **kwargs):
    print(assign_to, " = \n\t(",
          ",\n\t".join(map(str, v)),
          "\n\t);")


def unit_tests():
    pass

if __name__ == '__main__':
    unit_tests()
