# SymYlm -- Symbolic Real Spherical Harmonics #

This is a Python module that uses SymPy (Symbolic Python) to derive and manipulate polynomial expressions for the real spherical harmonics, specifically

* Derive symbolic expressions for the real spherical harmonics in both cartesian and spherical coordinates.

* Confirm they are orthogonal on the unit sphere.

* Supports fully normalized, Schmidt semi-normalized, and unnormalized.  Unit or surface area (4pi) normalized.

* Determine the maximum value of each, which is used for MaxN and FuMa normalization.  If possible this is done symbolically, if that fails, the code falls back to a numeric solution.

* Produce vectorized NumPy functions using SymPy lambdify

* Plot them using matplotlib or MayaVi.

* Printout the SymPy expressions in the syntax of a number of different programming languages.  Code is included for printing in Faust, which is useful form making Ambisonic panner plugins.

## SymYlm command line ##

```
$ python symYlm.py --help
usage: symYlm.py [-h] [-v] [-s] [-c] [--horner] [--four_pi | --unity]
                 [--orthonormal | --seminormalized | --unnormalized | --max | --FuMa]
                 [--condon_shortley_phase]
                 [-t {c,faust,fortran,javascript,julia,latex,mathematica,mathml,matlab,none,octave,pretty,tree}]
                 degree order

returns a polynomial experession for the real spherical harmonic of the
specified degree (l) and order (m)

positional arguments:
  degree                l, degree of the spherical harmonic. l>=0
  order                 m, order of the spherical harmonic. |m|<=l

optional arguments:
  -h, --help            show this help message and exit
  -v, --verbose         increase output verbosity
  -s, --spherical       use spherical coordinates.
  -c, --cartesian       use Cartesian coordinates. (default)
  --horner              use Horner's method to produce a more numerically-
                        stable expression
  --four_pi             inner product is 4pi
  --unity               inner product is 1 (default)
  --orthonormal         fully orthonormalized (default)
  --seminormalized      Schmidt seminormalized
  --unnormalized        no normalization
  --max                 max gain is 1 (note: may take a while to compute and
                        result may not exact for l>3
  --FuMa                Furse-Malham normalization
  --condon_shortley_phase
                        Use Condon-Shortley Phase (default is off)
  -t {c,faust,fortran,javascript,julia,latex,mathematica,mathml,matlab,none,octave,pretty,tree}, --translation {c,faust,fortran,javascript,julia,latex,mathematica,mathml,matlab,none,octave,pretty,tree}
```

### Example ###

To use the Ambix conventions[^1], specify --seminormalized and --four_pi:
```
bash-3.2$ python symYlm.py --seminormalized --four_pi 1 1
x

bash-3.2$ python symYlm.py --seminormalized --four_pi --spherical 1 1
cos(phi)*cos(theta)

bash-3.2$ python symYlm.py --seminormalized --four_pi 5 -3
sqrt(70)*(9*z**2 - 1)*(3*x**2*y - y**3)/16
```
### Plotting ###
```
bash-3.2$ python plot_Ylm.py 1 -1
```
![Ylm_1_-1.png](https://bitbucket.org/repo/764BBa/images/3440231472-Ylm_1_-1.png)

```
bash-3.2$ python plot_Ylm.py 2 1
```
![Ylm_2_1.png](https://bitbucket.org/repo/764BBa/images/1283136857-Ylm_2_1.png)

```
bash-3.2$ python plot_Ylm.py 3 2
```
![Ylm_3_2.png](https://bitbucket.org/repo/764BBa/images/742634110-Ylm_3_2.png)

### Unit Tests ###
The inner product of two spherical harmonics over the surface of the unit sphere should be 1 when l1=l2 and m1=m2, 0 otherwise.  The multiprocessing library is used to take advantage of multicore machines.   The four columns in the output are: (l1, m1), (l2, m2), inner product, run time (sec), status.
```
bash-3.2$ python ./symYlm_unit_test.py 4
Performing 136 tests...
(0, +0), (0, +0),  1.000000e+00, 0.12  PASS
(1, +0), (0, +0),  0.000000e+00, 0.22  PASS
(1, +0), (1, +0),  1.000000e+00, 0.24  PASS
(1, +1), (0, +0),  0.000000e+00, 0.26  PASS
(1, -1), (0, +0),  0.000000e+00, 0.27  PASS
(1, +0), (1, -1),  0.000000e+00, 0.32  PASS
(1, +1), (1, -1),  0.000000e+00, 0.34  PASS
(1, -1), (1, -1),  1.000000e+00, 0.36  PASS
(1, +1), (1, +0),  0.000000e+00, 0.24  PASS
(1, +1), (1, +1),  1.000000e+00, 0.27  PASS
(2, -2), (0, +0),  0.000000e+00, 0.33  PASS
(2, -2), (1, -1),  0.000000e+00, 0.35  PASS
(2, -2), (1, +0),  0.000000e+00, 0.37  PASS
(2, -2), (1, +1),  0.000000e+00, 0.35  PASS
(2, -2), (2, -2),  1.000000e+00, 0.42  PASS
...
(3, +3), (3, +3),  1.000000e+00, 0.76  PASS
(3, +1), (3, -3),  4.140895e-18, 12.26  PASS
(3, +3), (3, -1),  1.038093e-17, 9.59  PASS
(3, +3), (1, -1), -3.750538e-17, 28.04  PASS
(3, +2), (3, -2), -1.975352e-17, 29.66  PASS
(3, +3), (2, -2), -1.655338e-17, 87.28  PASS
Total time = 99.830062 secs.
```
## Dependancies ##

I use the Anaconda distribution of Python which includes SymPy.  This code has been tested with Python 2.7.10 and SymPy 0.7.6.

## License ##

This is published under the terms of the GNU Affero General Public License version 3.  If this is an impediment to your use of this code, please contact the author with the details of your application.

### Author

* Aaron J. Heller <heller@ai.sri.com>

## References ##

[^1]: C. Nachbar, F. Zotter, E. Deleflie, and A. Sontacchi, "AMBIX - A SUGGESTED AMBISONICS FORMAT," presented at the 3rd International Symposium on Ambisonics and Spherical Acoustics, Lexington, KY, 2011.
